import axios from 'axios'

const instance = axios.create({
  baseURL: 'http://localhost:8000'
})
const http = instance

function PredicCustomer(data: any) {
  return http.put('/Customer', data)
}

export default { PredicCustomer }
